﻿using System.Linq;
using System.Web.Mvc;
using ChatNET.Models;
using PusherServer;
using System.Web.Helpers;

namespace ChatNET.Controllers
{
    public class AuthController : Controller
    {
        [HttpPost]
        public JsonResult Login()
        {
            string username = Request.Form["username"];
            string password = Request.Form["password"];
            if (username == null || password == null)
            {
                return Json(new { status = "error", message = "Invalid credentials." });
            }
            if (username.Trim() == "" || password.Trim() == "")
            {
                return Json(new { status = "error", message = "Invalid credentials." });
            }
            
            using (var db = new Models.ChatContext())
            {
                Users user = db.Users.FirstOrDefault( x => x.name.Equals(username));
                if (user == null)
                {
                    return Json(new { status = "error", message = "User not registered" });
                }

                //Validate the entered credentials (password)
                var hashCode = user.vCode;
                var encodingPasswordString = RegistrationHelper.EncodePassword(password,hashCode);
                if (!encodingPasswordString.Equals(user.password))
                {
                    return Json(new { status = "error", message = "The entered password is not valid." });
                }
                Session["user"] = user;
            }
            return Json(new { status = "success", message = "" });
        }

        [HttpPost]
        public JsonResult Register()
        {
            string username = Request.Form["username"];
            string password = Request.Form["password"];
            if (username == null || password == null)
            {
                return Json(new { status = "error", message = "Invalid credentials." });
            }
            if (username.Trim() == "" || password.Trim() == "")
            {
                return Json(new { status = "error", message = "Invalid credentials." });
            }

            using (var db = new Models.ChatContext())
            {
                Users user = db.Users.FirstOrDefault(x => x.name.Equals(username));
                if(user != null)
                {
                    return Json(new { status = "error", message = "User already registered." });
                }
                else
                {
                    var keyNew = RegistrationHelper.GeneratePassword(10);
                    var pass = RegistrationHelper.EncodePassword(password,keyNew);
                    user = new Models.Users()
                    {
                        name = username,
                        password = pass,
                        vCode = keyNew //Store the verification code
                    };
                    db.Users.Add(user);
                    db.SaveChanges();
                    Session["user"] = user;
                    return Json(new { status = "success", message = "" });
                }
            }
        }


        public JsonResult AuthForChannel(string channel_name, string socket_id)
        {
            //Check is the session is already initialized
            if (Session["user"] == null)
            {
                return Json(new { status = "error", message = "Disconnected user." });
            }
            //Store the current user
            var currentUser = (Models.Users)Session["user"];
            
            //Initialize the pusher configuration
            var options = new PusherOptions();
            options.Cluster = "us2";
            var pusher = new Pusher(
                "1283456",
                "b03b171259ec750625cf",
                "565e99dacc7c465d0660",
                options
            );

            if (channel_name.IndexOf(currentUser.id.ToString()) == -1)
            {
                return Json(new { status = "error", message = "Couldn't join to this channel." });
            }

            var auth = pusher.Authenticate(channel_name, socket_id);

            return Json(auth);
        }

        public ActionResult Logout()
        {
            Session["user"] = null;
            return Redirect("/");
        }
    }
}