﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ChatNET.Models;
using PusherServer;

namespace ChatNET.Controllers
{
    public class ChatController : Controller
    {
        private Pusher pusher;
        public ChatController()
        {
            var options = new PusherOptions();
            options.Cluster = "us2";

            pusher = new Pusher(
                "1283456",
                "b03b171259ec750625cf",
                "565e99dacc7c465d0660",
                options
            );
        }

        public ActionResult Index()
        {
            if (Session["user"] == null)
            {
                return Redirect("/");
            }

            var currentUser = (ChatNET.Models.Users)Session["user"];

            using (var db = new ChatNET.Models.ChatContext())
            {
                ViewBag.allUsers = db.Users.Where(x => x.name != currentUser.name).ToList();
            }
            ViewBag.currentUser = currentUser;
            return View();
        }
                
        public JsonResult ConversationBtwContacts(int contact)
        {
            if (Session["user"] == null)
            {
                return Json(new { status = "error", message="Not connected user"});
            }

            var currentUser = (Models.Users)Session["user"];
            var chatConversations = new List<Models.ChatConversation>();

            using (var db = new ChatContext())
            {
                //Sort and take the last 50 chats between the defined contacts
                chatConversations = db.Conversation.Where(x => 
                    (x.receiver_id == currentUser.id && x.sender_id == contact) || 
                    (x.sender_id == currentUser.id && x.receiver_id == contact)
                ).OrderBy(x => x.createdAt).Take(50).ToList(); 

                return Json(new { status = "success", data = chatConversations},JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SendMessage()
        {
            if (Session["user"] == null)
            {
                return Json(new { status="error", message = "Not connected user" });
            }

            var currentUser = (Models.Users)Session["user"];
            string socketId = Request.Form["socket_id"];
            var contact = Convert.ToInt32(Request.Form["contact"]);
            string sentMessage = Request.Form["message"];

            if (contact == -1 || sentMessage.Trim().Length == 0)
            {
                //No contact selected
                return Json(new { status = "error", message = "No contact selected." });
            }


            ChatConversation conversation = new ChatConversation {
                sender_id = currentUser.id,
                receiver_id = contact,
                message = sentMessage
            };

            using (var db = new Models.ChatContext())
            {
                db.Conversation.Add(conversation);
                db.SaveChanges();
            }

            var conversationChannel = getConversationChannel(currentUser.id, contact);

            pusher.TriggerAsync(
                conversationChannel,
                "new_message",
                conversation,
                new TriggerOptions() { SocketId = socketId }
                );

            return Json(conversation);
        }

        public async Task<JsonResult> BotCall()
        {
            try
            {
                var client = new System.Net.Http.HttpClient();
                var stock = Request.Form["stock"];
                var command = Request.Form["command"];

                //Check if non-valid command was requested to the bot
                if (stock.Equals("NA") || command.Equals("NA"))
                {
                    //Invalid stock or command defined
                    return Json(new { status = "error", message = "Invalid command." });
                }

                //Prepare the request to the stooq site
                Stream stream = await client.GetStreamAsync("https://stooq.com/q/l/?s=" + stock + "&f=sd2t2ohlcv&h&e=csv");
                //Parse the csv content
                using (var reader = new StreamReader(stream))
                {
                    //Read the stream header
                    var header = reader.ReadLine();

                    //Read the data
                    var data = reader.ReadLine();
                    var value = data.Split(',');
                
                    string stockName = value[0];

                    double price = double.NaN;
                    double.TryParse(value[6], out price);
                
                    return Json(new { sender_id = "BOT", status = "success", stock_name = stockName, stock_price = price, message = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { status = "error", message = "The requested command couldn't be processed."});
            }

        }

        private String getConversationChannel(int userId, int contactId)
        {
            if (userId > contactId)
            {
                return "private-chat-" + contactId + "-" + userId;
            }
            return "private-chat-" + userId + "-" + contactId;
        }
    }
}