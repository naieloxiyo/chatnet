﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ChatNET.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["user"] != null)
            {
                return Redirect("/chat");
            }
            return View();
        }
        public  ActionResult Contact()
        {
            return View();
        }
    }
}