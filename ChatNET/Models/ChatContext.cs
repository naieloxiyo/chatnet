﻿using System;
using System.Data.Entity;

namespace ChatNET.Models
{
    public class ChatContext : DbContext
    {
        public ChatContext() : base("ChatNetDBContexts")
        {

        }

        public static ChatContext Create()
        {
            return new ChatContext();
        }

        public DbSet<Users> Users { get; set; }
        public DbSet<ChatConversation> Conversation { get; set; }
    }
}