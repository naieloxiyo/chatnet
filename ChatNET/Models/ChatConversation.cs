﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatNET.Models
{
    public class ChatConversation
    {
        public enum conversationStatus
        {
            Read,
            Sent,
            Delivered
        }
        public ChatConversation()
        {
            status = conversationStatus.Sent;
            createdAt = DateTime.Now;
            lastUpdateAt = DateTime.Now;
        }

        public int id { get; private set; }
        public int sender_id { get; set; }
        public int receiver_id { get; set; }
        public string message { get; set; }
        public conversationStatus status { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime lastUpdateAt { get; set; }
    }
}