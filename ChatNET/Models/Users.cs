﻿using System;
using System.Collections.Generic;

namespace ChatNET.Models
{
    public class Users
    {
        public Users()
        {
            created_at = DateTime.Now;
        }
        public int id { get; set; }
        public string name { get; set; }
        public DateTime created_at { get; set; }
        public string password { get; set; }
        public string vCode { get; set; }

    }
}