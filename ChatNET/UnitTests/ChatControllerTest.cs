﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChatNET.UnitTests
{
    [TestClass]
    public class ChatControllerTest
    {
        [TestMethod]
        public void TestBot()
        {
            var controller = new Controllers.ChatController();
            var result = controller.BotCall();
            result.Wait();
            var queryResult = result.Result;
            var field = queryResult.Data.GetType().GetProperty("status", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            
            //Check that the request returns an error in its json status because any stock was defined for its request
            Assert.AreEqual(field.GetValue(queryResult.Data,null), "error");
        }
    }
}